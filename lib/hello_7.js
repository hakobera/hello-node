/**
 * ファイル読み込み処理を別ファイルに分離する。
 * 独自モジュールの作成のサンプルです。
 */
var http = require('http'),
    hello = require('hello_7_module.js');
var server = http.createServer();
server.on('request', function(req, res) {
  res.writeHeader(200, { 'Content-Type': 'text/plain' });
  hello.on('data', function(data) {
    res.write(data);
    res.end();
  });
  hello.read();
});
server.listen(3000);
console.log('Server running at http://localhost:3000/');
