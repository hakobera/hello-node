/**
 * モジュールのサンプル。
 * hello.txt というファイルを読み込んでその内容をレスポンスとして返す
 */

var fs = require('fs');

module.exports = function(req, res) {
  var body = '';
  stream = fs.createReadStream(__dirname + '/hello.txt', { encoding: 'utf-8' });
  stream.on('data', function(data) {
    body += data;
  });
  stream.on('end', function() { 
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write(body);
    res.end();
  }, 3000);
};
