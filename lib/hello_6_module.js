/**
 * モジュールのサンプル。
 * hello.txt というファイルを読み込んでその内容をレスポンスとして返す。
 * ファイルの出力を response に pipe でつないだ例。
 */

var fs = require('fs');

module.exports = function(req, res) {
  var body = '';
  stream = fs.createReadStream(__dirname + '/hello.txt', { encoding: 'utf-8' });
  stream.pipe(res);
  res.writeHead(200, { 'Content-Type': 'text/plain' });
};
