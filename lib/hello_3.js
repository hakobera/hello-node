/**
 * コールバックを理解するために、
 * setTimeout を使って３秒後にレスポンスを返すようにする
 */
var http = require('http');
var server = http.createServer();
server.on('request', function(req, res) {
  setTimeout(function() {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write('Hello ');
    res.end('World\n');
  }, 3000);
});
server.listen(3000);
console.log('Server running at http://localhost:3000/');  
