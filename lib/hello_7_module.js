/**
 * モジュールのサンプル。
 * hello.txt というファイルを読み込んでその内容をレスポンスとして返す。
 * EventEmitter のサンプル。
 */

var fs = require('fs'),
    EventEmitter = require('events').EventEmitter;

var fileReader = new EventEmitter();
fileReader.read = function() {
  fs.readFile(__dirname + '/hello.txt', 'utf-8', function(err, data) {
    if (err) {
      throw new Error(err.message);
    }
    fileReader.emit('data', data);
  });
};

module.exports = fileReader;
