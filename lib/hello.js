var http = require('http');

var server = module.exports = http.createServer(function(req, res) {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.write('Hello ');
  res.end('World\n');
});

if (require.main === module) {
  server.listen(3000);
  console.log('Server running at http://localhost:3000/');
}
