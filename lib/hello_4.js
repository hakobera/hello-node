/**
 * 指定したファイルの内容を返すようにします。
 * hello.txt というファイルを読み込んでその内容をレスポンスとして返す
 */
var http = require('http'),
    fs = require('fs');
var server = http.createServer();
server.on('request', function(req, res) {
  var body = '';
  stream = fs.createReadStream(__dirname + '/hello.txt', { encoding: 'utf-8' });
  stream.on('data', function(data) {
    body += data;
  });
  stream.on('end', function() { 
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write(body);
    res.end();
  }, 3000);
});
server.listen(3000);
console.log('Server running at http://localhost:3000/');  
