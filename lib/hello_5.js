/**
 * ファイル読み込み処理を別ファイルに分離する。
 * 独自モジュールの作成のサンプルです。
 */
var http = require('http'),
    hello = require('hello_5_module.js');
var server = http.createServer();
server.on('request', function(req, res) {
  hello(req, res);
});
server.listen(3000);
console.log('Server running at http://localhost:3000/');  
