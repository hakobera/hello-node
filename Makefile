TESTS = test/*.js

test:
			@NODE_ENV=test ./node_modules/.bin/mocha \
							--require should \
							--slow 20 \
							$(TESTS)

.PHONY: test