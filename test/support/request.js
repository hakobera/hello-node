var http = require('http'),
    url = require('url'),
    util = require('util'),
    EventEmitter = require('events').EventEmitter;

/**
 * http.ClientRequest thin-wrapper class for unit test.
 * 
 * @param base Base URL
 * @constructor
 */
function Request(base) {
  this.base = base;
  this.res = null;
  EventEmitter.call(this);
}
util.inherits(Request, EventEmitter);

/**
 * Send GET request to specified path.
 * 
 * @param path URL (abolute path or relative path)
 * @return this (Request)
 */
Request.prototype.get = function(path) {
  if (this.base && path.substring(0, 1) === '/') {
    path = this.base + path;
  }

  var self = this,
      u = url.parse(path),
      options = {
        host: u.hostname,
        port: u.port,
        path: u.path
      };

  console.log(options);

  http.get(options, function(res) {
    if (res.statusCode === 200) {
      var body = '';
      res.on('data', function(chunk) {
        body += chunk;
      });
      res.on('end', function() {
        self.emit('success', body);
      });
    } else {
      self.emit('error', res);
    };
  }).on('error', function(e) {
    self.emit('error', e);
  });

  return this;
};

Request.prototype.success = function(callback) {
  this.on('success', callback);
};

Request.prototype.error = function(callback) {
  this.on('error', callback);
};

module.exports = Request;