var server = require('../lib/hello.js'),
    Request = require('./support/request');

var port = 8000;
var request;

describe('Hello World', function() {
  before(function(done) {
    request = new Request('http://localhost:' + port);
    server.listen(port, function() { done(); });
  });

  describe('GET /', function() {
    it('should return "Hello World"', function(done) {
      request
        .get('/')
        .success(function(data) {
          data.should.equal('Hello World\n');
          done();
        });
    });
  });

  after(function() {
    server.close();
  });
});